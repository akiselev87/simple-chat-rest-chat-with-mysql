package xml.message;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import config.Config;
import xml.friends.FriendManager;

public class MessageManager {
	public List<Message> getAllRecievedMessages(String securityKey) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String messageText = "";
		int messageId = -1;
		ArrayList<Message> allRecievedMessages= null;
		allRecievedMessages  = new ArrayList<Message>();
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery("select m.id,u.first_name, u.last_name, m.message from message m inner join users_link ul ON ul.id=m.link_id inner join users u ON u.id=m.user_id where ((ul.related_user="+userid+" or ul.id_user="+userid+")  AND (u.id!="+userid+" and ul.link_type=2))");
		while (resultset.next()){
			messageId = resultset.getInt("id");
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			messageText = resultset.getString("message");
			
			Message message = new Message(messageId,fname, lname, messageText, -1);
			allRecievedMessages.add(message);
		}
		return allRecievedMessages;
	}
	
	public List<Message> getAllSentMessages(String securityKey) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String messageText = "";
		int messageId = -1;
		ArrayList<Message> allSentMessages= null;
		allSentMessages  = new ArrayList<Message>();
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery("select m.id,u.first_name, u.last_name, m.message from message m inner join users_link ul ON ul.id=m.link_id inner join users u ON u.id=m.user_id where m.user_id="+userid+"");
		
		while (resultset.next()){
			messageId = resultset.getInt("id");
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			messageText = resultset.getString("message");
			Message message = new Message(messageId,fname, lname, messageText, -1);
			allSentMessages.add(message);
		}
		return allSentMessages;
	}
	
	
	public List<Message> getDialog(String securityKey, int senderid, int secureduserid) throws SQLException, ClassNotFoundException {
		String messageText = "";
		int messageId = -1;
		int authorId = -1;
		int userid = -1;
		ArrayList<Message> dialog= null;
		dialog  = new ArrayList<Message>();
		Config config = new Config();
		Statement statement = config.getStatement();
		if (secureduserid < 0){
			userid = config.getUserIdBySessionId(securityKey);
		} else
		{
			userid = secureduserid;
		}
		
		ResultSet resultset = statement.executeQuery("select m.id,m.user_id, m.message from message m inner join users_link ul ON ul.id=m.link_id inner join users u ON u.id=m.user_id where (ul.related_user="+senderid+" or ul.id_user="+senderid+")  AND ((u.id!="+userid+" and ul.link_type=2) OR (m.user_id="+userid+"));");
		while (resultset.next()){
			messageId = resultset.getInt("id");
			authorId = resultset.getInt("user_id");
			messageText = resultset.getString("message");
			/*
			This should be applied on the GUI
			if (userid==senderid){
			outcomeIncome = "sent";
			}
			else{
				outcomeIncome = "recieved";
			}
			*/
			Message message = new Message(messageId,"none", "none", messageText, authorId);
			dialog.add(message);
		}
	return dialog;
	}	
	
	public List<Message> deleteMessage(String securityKey, int senderid, int messageid) throws ClassNotFoundException, SQLException {
		List<Message> dialog = null;
		Config config = new Config();
		int userid = config.getUserIdBySessionId(securityKey);
		
		Statement statement = config.getStatement();
		if (userid > 0 && senderid > 0 && messageid > 0){
			int rowsAffected = statement.executeUpdate("DELETE FROM message WHERE id = " + messageid + ";");
			if(rowsAffected > 0) {
				dialog = getDialog(securityKey,senderid,userid);
			}
			else
			{
				//dialog= null;
			}
		}
		return dialog;
	}
	
			
	public void sendMessage(String securityKey, int recipientId, String messageText) throws SQLException, ClassNotFoundException {
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
	   /*if(recipientId.isnull){
		errorExists = 1;
		errorText = "Please specify recipient";
		return errorText;
		break;
	}
    
    if(message.equals("")){
		errorExists = 1;
		errorText = "Please, add message text";
		return errorText;
		break;
	}
	out.println(userid + "to" + recipient +"  "+ message);
    
	//check that this user is friend
	if (errorExists == 0){
	//get link id
	ResultSet rs;
	rs = statement.executeQuery("select users_link.id from users_link where ((id_user = "+userid+" and related_user = "+recipient+") OR (id_user = "+recipient+" and related_user = "+userid+")) ;");
    while (rs.next()){
		linkid = rs.getString("id");
		break;
    }
    
    if (linkid.equals("")){
		out.println("This user is not your friend<br>");
	}
	*/
	
	//statement uses linkid code
	FriendManager friendManager = new FriendManager();
	int linkId = friendManager.getUserRelationLinkIdForExactFriend(userid, recipientId);
	statement.executeUpdate("INSERT INTO message (link_id,user_id,message)VALUES ('" + linkId + "','" + recipientId + "','"+messageText+"');");
	}
}
