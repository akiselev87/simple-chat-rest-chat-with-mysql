package rest.message;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/MessageService")
public class MessageService {

   MessageManager messageManager = new MessageManager();

   @GET
   @Path("/allSentMessages/{securityKey}")
   @Produces(MediaType.APPLICATION_JSON)
   public String getSentMessages(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return messageManager.getAllSentMessages(securityKey);
   }	
   
   @GET
   @Path("/allRecievedMessages/{securityKey}")
   @Produces(MediaType.APPLICATION_JSON)
   public String getRecievedMessages(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return messageManager.getAllRecievedMessages(securityKey);
   }
   
   @GET
   @Path("/getDialog/{securityKey}/{friendid}")
   @Produces(MediaType.APPLICATION_JSON)
   public String getDialog(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return messageManager.getDialog(securityKey,friendid,-1);
   }
   
   @GET
   @Path("/deleteMessage/{securityKey}/{friendid}/{messageid}")
   @Produces(MediaType.APPLICATION_JSON)
   public String deleteMessage(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid,@PathParam("securityKey") int messageid) throws SQLException, ClassNotFoundException{
      return messageManager.deleteMessage(securityKey,friendid,messageid);
   }
   
   @GET
   @Path("/sendMessage/{securityKey}/{friendid}/{message}")
   //@Produces(MediaType.APPLICATION_JSON)
   public String sendMessage(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid,@PathParam("message") String message) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   //String q = "random word �500 bank $";
	   //String url = "http://example.com/query?q=" + URLEncoder.encode(q, "UTF-8");
	   message = URLDecoder.decode(message,"UTF-8");
	   return messageManager.sendMessage(securityKey,friendid,message);
   } 
}