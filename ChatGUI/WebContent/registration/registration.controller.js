(function () {
    'use strict';

angular.module('scopeRegistration', []).controller('RegController', ['$scope', '$http', function($scope, $http) {
    	
$scope.reset = function () {
    $scope.fname = null;
    $scope.lname = null;
    $scope.email = null;
    $scope.password = null;
    $scope.passwordConfirm = null;
    $scope.fname = null;
    $scope.fname = null;
    $scope.regForm.$setUntouched();
    regForm.fname.$touched = false;
};


$scope.register = function () {
	var email = escape($scope.email,"UTF-8");
	var pwd = escape($scope.password,"UTF-8");
	var pwdconfirm = escape($scope.passwordConfirm,"UTF-8");
	var fname = $scope.fname;
	var lname = $scope.lname;

	var urlRegistration = "http://localhost:8080/ChatRest/rest/AccountService/register/"+fname+"/"+lname+"/"+email+"/"+pwd+"/"+pwdconfirm;
		
	var responsePromise = $http.get(urlRegistration);	
		responsePromise.success
		(function(data, status, headers, config) {
			var errorText = data;
			if (errorText == ""){
				alert ("Registration Completed");
				window.open('../login/login.view.html','_self');
			
			}
			else
			{
				$scope.error = errorText;
			}
			});
	    responsePromise.error(function(data, status, headers, config) {
	        alert("Something went wrong with our database... Please, try again later");
	    });	
};
}]);
}());