package rest.friends;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
//import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;


@Path("/FriendService")
public class FriendService {
   FriendManager friendManager = new FriendManager();

   @GET
   @Path("/allFriends/{securityKey}")
   @Produces(MediaType.APPLICATION_JSON)
   public String getFriends(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return friendManager.getAllFriends(securityKey);
   }
   
   @GET
   @Path("/getAllFriendshipRequests/{securityKey}")
   @Produces(MediaType.APPLICATION_JSON)
   public String getAllFriendshipRequests(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return friendManager.getAllFriendshipRequests(securityKey);
   }
   
   @GET
   @Path("/getAllFriendIncludingWhoIgnoresCurrentgetExactFriend/{securityKey}")
   @Produces(MediaType.APPLICATION_JSON)
   public String getAllFriendIncludingWhoIgnoresCurrentgetExactFriend(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return friendManager.getAllFriendIncludingWhoIgnoresCurrentgetExactFriend(securityKey);
   }
   
   @GET
   @Path("/getWhoIgnoresMe/{securityKey}")
   @Produces(MediaType.APPLICATION_JSON)
   public String getWhoIgnoresMe(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return friendManager.getWhoIgnoresMe(securityKey);
   }
   
   @GET
   @Path("/getExactFriendData/{securityKey}/{friendid}")
   @Produces(MediaType.APPLICATION_JSON)
   public String getExactFriend(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.getExactFriend(securityKey,friendid);
   }
   
   /*@GET
   @Path("/validateNewFriendIsAlreadyFriend/{securityKey}/{friendid}")
   public int validateNewFriendIsAlreadyFriend(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.validateNewFriendIsAlreadyFriend(securityKey, friendid);
   }
   */
   
   @GET
   @Path("/getUserRelationLinkIdForExactFriend/{securityKey}/{friendid}")
   public int validateExactFriend(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.getUserRelationLinkIdForExactFriend(securityKey, friendid);
   }
   
   @GET
   @Path("/getNewFriendId/{email}")
   public int getNewFriendId(@PathParam("email") String email) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   email = URLDecoder.decode(email,"UTF-8");
	   return friendManager.getNewFriendId(email);
   }
   
   @GET
   @Path("/ignoreFriend/{securityKey}/{friendid}")
   public int ignoreFriend(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.ignoreFriend(securityKey, friendid);
   }
   
   @GET
   @Path("/restoreFriend/{securityKey}/{idFriend}")
   public int restoreFriend(@PathParam("securityKey") String securityKey,@PathParam("idFriend") int idFriend) throws SQLException, ClassNotFoundException{
      return friendManager.restoreFriend(securityKey, idFriend);
   }
   
   @GET
   @Path("/addFriend/{securityKey}/{friendEmail}")
   public String addFriend(@PathParam("securityKey") String securityKey, @PathParam("friendEmail") String friendEmail) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   friendEmail = URLDecoder.decode(friendEmail,"UTF-8");
	   return friendManager.addFriend(securityKey, friendEmail);
   }
   
   /*
   @GET
   @Path("/getFriendshipRequistUserLinkId/{securityKey}/{friendid}")
   public int getFriendshipRequistUserLinkId(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.getFriendshipRequestUserLinkId(securityKey, friendid);
   }
   */
   
   @GET
   @Path("/approveFriendshipRequest/{securityKey}/{friendid}")
   public int approveFriendshipRequest(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.approveFriendshipRequest(securityKey, friendid);
   }
}