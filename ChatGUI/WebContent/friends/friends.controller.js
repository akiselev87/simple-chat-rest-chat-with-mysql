(function () {
    'use strict';

angular.module('myAppName', []).controller('ContactController', ['$scope', '$http', '$interval', function($scope, $http, $interval) {
	var securityKey = cookieGet("securityKey");
	var host = cookieGet("host");
	//$scope.newcontactEmail = "Your friend e-mail";
	//onfocus="if(this.value == 'Your friend e-mail') { this.value = ''; }" value="Your friend e-mail"

	$scope.getAllIgnoredUsers = function getAllIgnoredUsers(){	
		//GET ALL IGNORED USERS LIST
		var urlGetAllFriendIncludingWhoIgnoresCurrentgetExactFriend = "http://" + host + "/ChatRest/rest/FriendService/getWhoIgnoresMe/"+securityKey;
		var responsePromise3 = $http.get(urlGetAllFriendIncludingWhoIgnoresCurrentgetExactFriend);	
		responsePromise3.success
		(function(data, status, headers, config) {
			/*
			XML to JSON conversion
			var xmlResponse1 = data;
			var x2js1 = new X2JS();
			var jsonResponse1 = x2js1.xml_str2json(xmlResponse1);
		    $scope.ignores = jsonResponse1.friends.friend;
		    */
			$scope.ignores = data;
		});
	};

	$scope.getAllFriendshipRequests = function getAllFriendshipRequests(){
	//GET ALL REQUESTS LIST
	var urlAllRequests = "http://" + host + "/ChatRest/rest/FriendService/getAllFriendshipRequests/"+securityKey;
	var responsePromise2 = $http.get(urlAllRequests);	
	responsePromise2.success
		(function(data, status, headers, config) {
			/*
			XML to JSON conversion
			var xmlResponse1 = data;
			var x2js1 = new X2JS();
			var jsonResponse1 = x2js1.xml_str2json(xmlResponse1);
		    $scope.requests = jsonResponse1.friends.friend;
		    */
			$scope.requests = data;
		});
	};

	$scope.getAllFriends = function getAllFriends(){
		// GET ALL FRIENDS LIST
		var urlAllFriends = "http://" + host + "/ChatRest/rest/FriendService/allFriends/"+securityKey;
		var responsePromise1 = $http.get(urlAllFriends);	
		responsePromise1.success
			(function(data, status, headers, config) {
			/*
			XML to JSON conversion
			var xmlResponse1 = data;
			var x2js1 = new X2JS();
			var jsonResponse1 = x2js1.xml_str2json(xmlResponse1);
		    $scope.contacts = jsonResponse1.friends.friend;
		    */
			$scope.contacts = data;
		    });
		responsePromise1.error
			(function(data, status, headers, config) {
			alert("Request failed!");
		});	
	};

//BUTTONS
$scope.saveContact = function () {	
	$scope.friendAddError = "";
	$scope.friendAddValid = "";
	var friendEmail = $scope.newcontactEmail;
	var urlAddFriend = "http://" + host + "/ChatRest/rest/FriendService/addFriend/" + securityKey + "/" + friendEmail;
	var responsePromise4 = $http.get(urlAddFriend);	
	responsePromise4.success
		(function(data, status, headers, config) {
			if (data == ""){
				$scope.friendAddValid = "Request sent";
				//alert ("Request sent");
			}
			else
	    	{
				$scope.friendAddError = data;
	    	}
	    	});
	    responsePromise4.error(function(data, status, headers, config) {
	        alert("Request failed!");
	    });	
};

$scope.ignoreFriend = function(id) {
	var urlIgnoreFriend = "http://" + host + "/ChatRest/rest/FriendService/ignoreFriend/" + securityKey +"/" + id;
	
	if (confirm("Do you want to ignore friend?") == true) {
		var responsePromise5 = $http.get(urlIgnoreFriend);	
		responsePromise5.success
		(function(data, status, headers, config) {
			if (data = 1){
				$scope.friendIgnoreValid = "Ignored since this moment";
				$scope.getAllFriends();
				$scope.getAllFriendshipRequests();
				$scope.getAllIgnoredUsers();				
				//all reload functions could be optimized by excluding/adding items to the arrays
			}
			else
	    	{
				$scope.friendIgnoreError = "You can't ignore this guy";
	    	}
	    	});
	    responsePromise5.error(function(data, status, headers, config) {
	        alert("Request failed!");
	    });
    } else {
        //cancel
    }
}; 

$scope.approveFriend = function(idFriend) {
	var urlIgnoreFriend = "http://" + host + "/ChatRest/rest/FriendService/approveFriendshipRequest/" + securityKey + "/" + idFriend;
	
	var responsePromise6 = $http.get(urlIgnoreFriend);	
		responsePromise6.success
		(function(data, status, headers, config){
			if (data = 1){
				$scope.friendApproveValid = "Congratulations! You got a new friend";
				$scope.getAllFriends();
				$scope.getAllFriendshipRequests();
				//alert ("Request sent");
			}
			else
	    	{
				$scope.friendApproveError = "You can't add this guy";
	    	}
	    	});
	    responsePromise6.error(function(data, status, headers, config) {
	        alert("Request failed!");
	    });	
}; 


$scope.restoreFriend = function(idFriend) {
	var urlRestoreFriend = "http://" + host + "/ChatRest/rest/FriendService/restoreFriend/" + securityKey + "/" + idFriend;
	var responsePromise7 = $http.get(urlRestoreFriend);	
		responsePromise7.success
		(function(data, status, headers, config) {
			if (data = 1){
				$scope.friendUnIgnoreeValid = "Congratulations! You got a new friend";
				$scope.getAllFriends();
				$scope.getAllIgnoredUsers();
				//alert ("Request sent");
			}
			else
	    	{
				$scope.friendUnIgnoreError = "You can't add this guy";
	    	}
	    	});
	    responsePromise7.error(function(data, status, headers, config) {
	        alert("Request failed!");
	    });	
}; 

/*
 * 
 * CALL FOR FUNCTIONS ON PAGE LOAD
 * 
*/
$scope.getAllFriends();
$scope.getAllFriendshipRequests();
$scope.getAllIgnoredUsers();

var counterOfRequestsFriends=0;
var timerFriends=$interval(function(){
	if(counterOfRequestsFriends > 3)
      {
    	$scope.autoRefeshStatus = "Our server is tired. Please, start auto refreshing cycle manually ";
      }
    else{
    	$scope.getAllFriends();	
    	counterOfRequestsFriends++;
    }
  },50000);

var counterOfRequestFriendRequests=0;
var timer=$interval(function(){
	if(counterOfRequestFriendRequests > 3)
	  {
		$scope.autoRefeshStatus = "Our server is tired. Please, start auto refreshing cycle manually ";
	  }
	else{
		$scope.getAllFriendshipRequests();
		counterOfRequestFriendRequests++;
	}
  },10000);

$scope.autoRefresh = function () {
   $scope.autoRefeshStatus = "";
   counterOfRequestsFriends = 0;
   counterOfRequestFriendRequests = 0;
};
   
}]);
}());

/*
SAVE CONTACT FUNCTION
$scope.saveContact = function() {
    var i = 0;    
	if($scope.newcontact.id == null) {
             $scope.newcontact.id = uid++;
             $scope.contacts.push($scope.newcontact);
        } else {
            
             for(i in $scope.contacts) {
                    if($scope.contacts[i].id == $scope.newcontact.id) {
                        $scope.contacts[i] = $scope.newcontact;
                    }
             }                
        }
        $scope.newcontact = {};
};
*/

/*
SHOW CONFIRM BUTTON POP UP WINDOW WITH BUTTONS
$scope.showConfirm = function(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('Would you like to delete your debt?')
          .textContent('All of the banks have agreed to forgive you your debts.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Please do it!')
          .cancel('Sounds like a scam');
    $mdDialog.show(confirm).then(function() {
      $scope.status = 'You decided to get rid of your debt.';
    }, function() {
      $scope.status = 'You decided to keep your debt.';
    });
  };
*/	


