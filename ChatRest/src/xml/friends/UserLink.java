package xml.friends;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "UserLink")
public class UserLink implements Serializable {

   private static final long serialVersionUID = 1L;
   private int id;
   private int idUser;
   private int relatedUser;
   private int linkType;
   private String friendFirstName;
   private String friendLastName;

   public UserLink(){}
   
   public UserLink(int id, int idUser, int relatedUser, int linkType, String friendFirstName, String friendLastName){
      this.id = id;
      this.idUser = idUser;
      this.relatedUser = relatedUser;
      this.linkType = linkType;
      this.friendFirstName = friendFirstName;
      this.friendLastName = friendLastName;
   }
   //------------------------------
   public int getId() {
      return id;
   }

   @XmlElement
   public void setId(int id) {
      this.id = id;
   }
   
   //------------------------------
   public int getIdUser() {
	      return idUser;
	   }
   @XmlElement
   public void setIdUser (int idUser) {
	   this.idUser = idUser;
   }
   //------------------------------	   
   public int getRelatedUser() {
      return relatedUser;
   }
   @XmlElement
   public void setRelatedUser(int relatedUser) {
      this.relatedUser = relatedUser;
   }
   
 //------------------------------
   public int getLinkType() {
      return linkType;
   }
   @XmlElement
   public void setLinkType(int linkType) {
      this.linkType = linkType;
   }
   
 //------------------------------
   public String getFriendFirstName() {
      return friendFirstName;
   }
   @XmlElement
   public void setFriendFirstName(String friendFirstName) {
      this.friendFirstName = friendFirstName;
   }
   
 //------------------------------
   public String getFriendLastName() {
      return friendLastName;
   }
   @XmlElement
   public void setFriendLastName(String friendLastName) {
      this.friendLastName = friendLastName;
   }
}