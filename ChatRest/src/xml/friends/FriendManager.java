package xml.friends;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.codehaus.jackson.node.JsonNodeFactory;
import org.codehaus.jackson.node.ObjectNode;

import config.Config;


public class FriendManager {
	public List<Friend> getAllFriends(String securityKey) throws SQLException, ClassNotFoundException {
	Config config = new Config();
	Statement statement = config.getStatement();
	int friendId = -1;
	String fname = "";
	String lname = "";
	String email = "";
	ArrayList<Friend> friendList = null;
	friendList  = new ArrayList<Friend>();
	int userid = config.getUserIdBySessionId(securityKey);
	
	ResultSet resultset = statement.executeQuery("SELECT users.id,users.email,users.first_name,users.last_name,users.id FROM chat.users_link ul inner join chat.users on (CASE WHEN ul.id_user = " + userid + " and (ul.link_type=2 OR ul.link_type=3) THEN ul.related_user = users.id  WHEN ul.related_user = " + userid + " and (ul.link_type=2 OR ul.link_type=4) THEN ul.id_user = users.id END);");
	while (resultset.next()){
		friendId = resultset.getInt("id");
		fname = resultset.getString("first_name");
		lname = resultset.getString("last_name");
		email = resultset.getString("email");
		Friend friend = new Friend(friendId,fname, lname, email);
		friendList.add(friend);
		}
	return friendList;
	}
	
	/*
	 * old code with sql to rework
	public List<UserLink> selectUserLinksToIgnore(int userid) throws SQLException, ClassNotFoundException {
		Config config = new Config();
		Statement statement = config.getStatement();
		int friendId = -1;
		String friendFirstName = "";
		String friendLastName = "";
		int linkType = -1;
		int usersLinkId = -1;
		ArrayList<UserLink> userLinkList = null;
		userLinkList  = new ArrayList<UserLink>();
		
		ResultSet resultset = statement.executeQuery(
		"(SELECT users.first_name,users.last_name,users.id, users.email,ul.id as ulid, ul.id_user, ul.related_user,ul.link_type "
		+ "FROM chat.users_link ul inner join chat.users on (ul.id_user = users.id) "
		+ "where ((ul.id_user="+userid+ " or ul.related_user = "+ userid +") "
				+ "AND users.id !="+userid+" and ul.link_type!=4)) "
		+ "UNION"
		+ "(SELECT users.first_name,users.last_name,users.id, users.email,ul.id as ulid, ul.id_user, ul.related_user,ul.link_type "
		+ "FROM chat.users_link ul inner join chat.users on (ul.related_user = users.id) "
		+ "where ((ul.id_user="+userid+ " or ul.related_user = "+ userid +")"
				+ "AND users.id !="+userid+" and ul.link_type!=3));");
		
		while (resultset.next()){
			friendId = resultset.getInt("id");
			friendFirstName = resultset.getString("first_name");
			friendLastName = resultset.getString("last_name");
			usersLinkId = resultset.getInt("ulid");
			linkType = resultset.getInt("link_type");
			usersLinkId = resultset.getInt("related_user");
			
			UserLink userLink = new UserLink(usersLinkId,userid,friendId,linkType,friendFirstName,friendLastName); 
			//}
			userLinkList.add(userLink);
			//	i++;
			}
		return userLinkList;
	}
	*/
	
	public List<Friend> getAllFriendshipRequests(String securityKey) throws SQLException, ClassNotFoundException {
		Config config = new Config();
		Statement statement = config.getStatement();
		int friendId = -1;
		String fname = "";
		String lname = "";
		String email = "";
		ArrayList<Friend> friendList = null;
		friendList  = new ArrayList<Friend>();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery("SELECT u.first_name,u.email,u.last_name,ul.id as ulid FROM chat.users u inner join chat.users_link ul on ul.id_user = u.id where ((ul.related_user="+userid+" or ul.id_user="+userid+")  AND (u.id!=" +userid+" and ul.link_type=1))");
		while (resultset.next()){
			friendId = resultset.getInt("ulid");
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			email = resultset.getString("email");
			Friend friend = new Friend(friendId,fname, lname, email);
			friendList.add(friend);
			}
		return friendList;
	}
	
	public List<Friend> getAllFriendIncludingWhoIgnoresCurrentgetExactFriend(String securityKey) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String email = "";
		int friendId =-1;
		Config config = new Config();
		Statement statement = config.getStatement();
		ArrayList<Friend> friendList = null;
		friendList  = new ArrayList<Friend>();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery(
		"SELECT users.id,users.email,users.first_name,users.last_name,users.id "
		+"FROM chat.users_link ul inner join chat.users on "
		+"(CASE "
		+"	WHEN ul.id_user = " + userid + " THEN ul.related_user = users.id "
		+"	WHEN ul.related_user = " + userid + " THEN ul.id_user = users.id "
		+" END);");
		
		while (resultset.next()){
			friendId = resultset.getInt("id");
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			email = resultset.getString("email");
			Friend friend = new Friend(friendId,fname, lname, email);
			friendList.add(friend);
			}
		return friendList;
	}

	public List<Friend> getWhoIgnoresMe(String securityKey) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String email = "";
		int friendId =-1;
		Config config = new Config();
		Statement statement = config.getStatement();
		ArrayList<Friend> friendList = null;
		friendList  = new ArrayList<Friend>();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery(
		"SELECT users.id,users.email,users.first_name,users.last_name,users.id "
		+"FROM chat.users_link ul inner join chat.users on "
		+"(CASE "
		+"	WHEN ul.id_user = " + userid + " and (ul.link_type=3 AND ul.link_type=6) THEN ul.related_user = users.id "
		+"	WHEN ul.related_user = " + userid + " and (ul.link_type=4 OR ul.link_type=6) THEN ul.id_user = users.id "
		+" END);");
		while (resultset.next()){
			friendId = resultset.getInt("id");
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			email = resultset.getString("email");
			Friend friend = new Friend(friendId,fname, lname, email);
			friendList.add(friend);
			}
		return friendList;
	}

	
	
	public List<Friend> getExactFriend(String securityKey, int friendId) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String email = "";
		Config config = new Config();
		Statement statement = config.getStatement();
		ArrayList<Friend> friendList = null;
		friendList  = new ArrayList<Friend>();
		ResultSet resultset = statement.executeQuery("select u.id,u.first_name,u.last_name,u.email from users u where u.id=" + friendId + ";");
		if (resultset.first()){
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			email = resultset.getString("email");
			Friend friend = new Friend(friendId,fname, lname, email);
			friendList.add(friend);
			}
		return friendList;
	}
	
	public int getUserRelationLinkIdForExactFriend(int userid, int recipientId) throws SQLException, ClassNotFoundException {
		int linkId = -1; // 1 - exists, 0 - not exists
		Config config = new Config();
		Statement statement = config.getStatement();
		
		ResultSet resultset = statement.executeQuery(
		"select users_link.id from users_link "
		+ "where ((id_user = "+userid+" and related_user = "+recipientId+") "
				+ "OR (id_user = "+recipientId+" and related_user = "+userid+")) ;");
		if (resultset.first()){
			linkId = resultset.getInt("id");
		}
		return linkId;
	}
	
	public int validateNewFriendIsAlreadyFriend (int userid, int friendId) throws SQLException, ClassNotFoundException {
		int linkId = -1; // 1 - exists, 0 - not exists
		Config config = new Config();
		Statement statement = config.getStatement();
		
		ResultSet resultset = statement.executeQuery("SELECT ul.id FROM chat.users_link ul inner join chat.users on ul.related_user = users.id where (ul.id_user ='" + userid + "' AND ul.related_user='" + friendId + "')");// or (ul.id_user ='" + friendid + "' AND ul.related_user='" + currentUserid + "');");
		if (resultset.first()){
			linkId = resultset.getInt("id");
		}
		return linkId;
	}
	
	public int getNewFriendId(String email) throws SQLException, ClassNotFoundException {
		int friendId = -1; // 1 - exists, 0 - not exists
		Config config = new Config();
		Statement statement = config.getStatement();
		
		ResultSet resultset = statement.executeQuery("select id, email, first_name, last_name from users where email = '"+email+"';");
		if (resultset.first()){
			friendId = resultset.getInt("id");
		}
		return friendId;
	}
	
	public int ignoreFriend(String securityKey, int ulidForFriendship) throws SQLException, ClassNotFoundException {
		Config config = new Config();
		Statement statement = config.getStatement();
		int status = 0;
		int userid = config.getUserIdBySessionId(securityKey);
		
		status = status + statement.executeUpdate("update users_link set link_type = 3 where id="+ulidForFriendship+" and (link_type=3 or link_type=4);");
		status = status + statement.executeUpdate("update users_link set link_type = 3 where id="+ulidForFriendship+" and id_user="+userid+" and link_type !=5;");
		status = status + statement.executeUpdate("update users_link set link_type = 4 where id="+ulidForFriendship+" and related_user="+userid+" and link_type !=5;");
		if (status > 0){
			return 1;
		}
		else {
			return -1;
		}
		
	}
	
	public int restoreFriend(String securityKey, int ulidForFriendship) throws SQLException, ClassNotFoundException {
		Config config = new Config();
		Statement statement = config.getStatement();
		int status = 0;
		int userid = config.getUserIdBySessionId(securityKey);
		
		status = status + statement.executeUpdate("update users_link set link_type = 4 where id="+ulidForFriendship+" and id_user="+userid+" and link_type = 5;");
		status = status + statement.executeUpdate("update users_link set link_type = 3 where id="+ulidForFriendship+" and related_user="+userid+" and link_type = 5;");
		status = status + statement.executeUpdate("update users_link set link_type = 2 where id="+ulidForFriendship+" and id_user="+userid+" and link_type = 3;");
		status = status + statement.executeUpdate("update users_link set link_type = 2 where id="+ulidForFriendship+" and related_user="+userid+" and link_type = 4;");
		if (status > 0){
			return 1;
		}
		else {
			return -1;
		}
	}
	
	
	public int getFriendshipRequestUserLinkId(int userid, int friendId) throws SQLException, ClassNotFoundException {
		int userLinkId = -1;
		Config config = new Config();
		Statement statement = config.getStatement();
		
		ResultSet resultset = statement.executeQuery(
		"SELECT ul.id FROM chat.users_link ul "
		+ "inner join chat.users on ul.related_user = users.id "
		+ "where (ul.id_user ='" + friendId + "' AND ul.related_user='" + userid + "');");
		if (resultset.first()){
			userLinkId = resultset.getInt("id");
		}
		else{
			userLinkId = 0;
		}
		
		return userLinkId;
	}
	
	public String addFriend(String securityKey, String friendEmail) throws SQLException, ClassNotFoundException {
		//patterns
		Pattern pattern;
		Matcher matcher;
		String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			
		//variables
		int errorexists = 0;
		int friendId = -1;
		String errorText = "";
		
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
		
		// check that user specified e-mail
		if(friendEmail.equals("")){
			errorexists = 1;
			errorText = "Please, add E-Mail";	
		}

		//check e-mail pattern
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(friendEmail);
			if (!matcher.matches()) {
				errorexists = 1;
				errorText = errorText + "Please, verify e-mail format";
			}
		   	
		//get id of the new friend
		friendId = getNewFriendId(friendEmail);
		if (friendId== -1){
			errorexists = 1;
			errorText = errorText + "No user with this e-mail registered";
			}
				
		//verify that friend is not previously added
		if (errorexists == 0){
			int linkId = validateNewFriendIsAlreadyFriend(userid, friendId);
			if (linkId != -1){
				errorexists = 1;
				errorText ="Account with the same e-mail is already your friend or your friendship request is still pending.";
				}
			}
			
		//approve previously requested friendship
		if (errorexists == 0){
			int usersLinkkId = getFriendshipRequestUserLinkId(userid, friendId);
			if (usersLinkkId != 0){
			statement.executeUpdate("update users_link set link_type = 2 where id='" + usersLinkkId + "';");
			errorexists = 2;//friendship sucessfully approved
			}
		}
			
		//Insert new friendship request
		 if (errorexists == 0){
		   	statement.executeUpdate("INSERT INTO users_link (id_user, related_user, link_type) VALUES ('" + userid + "','"+ friendId +"','1');");
		 }
		 return errorText;
	}
	
	public int approveFriendshipRequest(int ulidForFriendship) throws SQLException, ClassNotFoundException {
		Config config = new Config();
		Statement statement = config.getStatement();
		
		int rowsAffected = statement.executeUpdate("update users_link set link_type = 2 where id="+ulidForFriendship+";");
		if (rowsAffected > 0){
			return 1;
		}
		else {
			return -1;
		}
	}

}