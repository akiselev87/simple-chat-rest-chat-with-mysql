package rest.message;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import config.Config;
import rest.friends.Friend;
import rest.friends.FriendManager;

public class MessageManager {
	private String convertArrayListToJSON(ArrayList<Message> messageList) {
		Gson gson = new Gson();
		String json = gson.toJson(messageList);
		return json;
	}
	
	public String getAllRecievedMessages(String securityKey) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String messageText = "";
		int messageId = -1;
		ArrayList<Message> allRecievedMessages= null;
		allRecievedMessages  = new ArrayList<Message>();
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery("select m.id,u.first_name, u.last_name, m.message from message m inner join users_link ul ON ul.id=m.link_id inner join users u ON u.id=m.user_id where ((ul.related_user="+userid+" or ul.id_user="+userid+")  AND (u.id!="+userid+" and ul.link_type=2))");
		while (resultset.next()){
			messageId = resultset.getInt("id");
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			messageText = resultset.getString("message");
			Message message = new Message(messageId,fname, lname, messageText, -1);
			allRecievedMessages.add(message);
		}
		String json = convertArrayListToJSON(allRecievedMessages);
	    return json;
	}
	
	public String getAllSentMessages(String securityKey) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String messageText = "";
		int messageId = -1;
		ArrayList<Message> allSentMessages= null;
		allSentMessages  = new ArrayList<Message>();
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery("select m.id,u.first_name, u.last_name, m.message from message m inner join users_link ul ON ul.id=m.link_id inner join users u ON u.id=m.user_id where m.user_id="+userid+"");
		while (resultset.next()){
			messageId = resultset.getInt("id");
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			messageText = resultset.getString("message");
			Message message = new Message(messageId,fname, lname, messageText, -1);
			allSentMessages.add(message);
		}
		String json = convertArrayListToJSON(allSentMessages);
	    return json;
	}
	
	
	public String getDialog(String securityKey, int senderid, int secureduserid) throws SQLException, ClassNotFoundException {
		String messageText = "";
		int messageId = -1;
		int authorId = -1;
		int userid = -1;
		ArrayList<Message> dialog= null;
		dialog  = new ArrayList<Message>();
		Config config = new Config();
		Statement statement = config.getStatement();
		if (secureduserid < 0){
			userid  = config.getUserIdBySessionId(securityKey);
		} else
		{
			userid = secureduserid;
		}
		
		ResultSet resultset = statement.executeQuery("select m.id,m.user_id, m.message from message m inner join users_link ul ON ul.id=m.link_id inner join users u ON u.id=m.user_id where (ul.related_user="+senderid+" or ul.id_user="+senderid+")  AND ((u.id!="+userid+" and ul.link_type=2) OR (m.user_id="+userid+")) order by m.id desc;;");
		while (resultset.next()){
			messageId = resultset.getInt("id");
			authorId = resultset.getInt("user_id");
			messageText = resultset.getString("message");
			Message message = new Message(messageId,"none", "none", messageText, authorId);
			dialog.add(message);
		}
		String json = convertArrayListToJSON(dialog);
	    return json;
	}	
	
	public String deleteMessage(String securityKey, int senderid, int messageid) throws ClassNotFoundException, SQLException {
		String dialog = null;
		Config config = new Config();
		int userid = config.getUserIdBySessionId(securityKey);
		Statement statement = config.getStatement();
		if (userid > 0 && senderid > 0 && messageid > 0){
			statement.executeUpdate("DELETE FROM message WHERE id = '" + messageid + "';");
			dialog = getDialog(securityKey,senderid,userid);
		}
		return dialog;
	}
			
	public String sendMessage(String securityKey, int recipientId, String messageText) throws SQLException, ClassNotFoundException {
		Config config = new Config();
		Statement statement = config.getStatement();
		int errorExists = 0;
		//int friendId = -1;
		String errorText = "";
		
		if(recipientId < 1){
			errorExists = 1;
			errorText = "Please specify recipient";
		}
	    
	    if(messageText.equals("")){
			errorExists = 1;
			errorText = "Please, add message text";
		}
		
		//check that this user is friend
		/*if (errorExists == 0){
			//get link id
			int linkId = -1;
			ResultSet rs;
			rs = statement.executeQuery(""
			+ "select users_link.id from users_link where "
			+ "((id_user = "+userid+" and related_user = "+recipientId+") "
			+ "OR "
			+ "(id_user = "+recipientId+" and related_user = "+userid+")) ;");
			if (rs.first()){
				 linkId = rs.getInt("id");
			}
			if (linkId < 1){
				errorExists = 1;
				errorText = "Please, add this person as a friend first";
		}
		
			if (errorExists == 0){
			   	statement.executeUpdate("INSERT INTO users_link (id_user, related_user, link_type) VALUES ('" + userId + "','"+ friendId +"','1');");
			 }
		*/
		//else{
	    
	    //check that this user is friend
		FriendManager friendManager = new FriendManager();
		int linkId = friendManager.getUserRelationLinkIdForExactFriend(securityKey, recipientId);
		if (linkId < 1){
			errorExists = 1;
			errorText = "Please, add this person as a friend first";
		}
		else
		{
			statement.executeUpdate("INSERT INTO message (link_id,user_id,message)VALUES "
					+ "('" + linkId + "','" + recipientId + "','"+messageText+"');");
		}
		return messageText;
		}	
	}
