package xml.friends;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;


@Path("/FriendService")
public class FriendService {
/*
 * <dependency>
			<groupId>org.json</groupId>
			<artifactId>json</artifactId>
			<version>20140107</version>
		</dependency>
		
 */
   FriendManager friendManager = new FriendManager();
   
   @GET
   @Path("/allFriends/{securityKey}")
   @Produces(MediaType.APPLICATION_XML)
   public List<Friend> getFriends(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return friendManager.getAllFriends(securityKey);
   }
   
   @GET
   @Path("/getAllFriendshipRequests/{securityKey}")
   @Produces(MediaType.APPLICATION_XML)
   public List<Friend> getAllFriendshipRequests(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return friendManager.getAllFriendshipRequests(securityKey);
   }
   
   @GET
   @Path("/getAllFriendIncludingWhoIgnoresCurrentgetExactFriend/{securityKey}")
   @Produces(MediaType.APPLICATION_XML)
   public List<Friend> getAllFriendIncludingWhoIgnoresCurrentgetExactFriend(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return friendManager.getAllFriendIncludingWhoIgnoresCurrentgetExactFriend(securityKey);
   }
   
   @GET
   @Path("/getWhoIgnoresMe/{securityKey}")
   @Produces(MediaType.APPLICATION_XML)
   public List<Friend> getWhoIgnoresMe(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
      return friendManager.getWhoIgnoresMe(securityKey);
   }
   
   @GET
   @Path("/getExactFriendData/{securityKey}/{friendid}")
   @Produces(MediaType.APPLICATION_XML)
   public List<Friend> getExactFriend(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.getExactFriend(securityKey,friendid);
   }
   
   /*@GET
   @Path("/validateNewFriendIsAlreadyFriend/{securityKey}/{friendid}")
   //@Produces(MediaType.APPLICATION_XML)
   public int validateNewFriendIsAlreadyFriend(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.validateNewFriendIsAlreadyFriend(securityKey, friendid);
   }
   */
   
   /*
   @GET
   @Path("/getUserRelationLinkIdForExactFriend/{securityKey}/{friendid}")
   //@Produces(MediaType.APPLICATION_XML)
   public int validateExactFriend(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.getUserRelationLinkIdForExactFriend(securityKey, friendid);
   }s
   */
   
   @GET
   @Path("/getNewFriendId/{email}")
   //@Produces(MediaType.APPLICATION_XML)
   public int getNewFriendId(@PathParam("email") String email) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   email = URLDecoder.decode(email,"UTF-8");
	   return friendManager.getNewFriendId(email);
   }
   
   @GET
   @Path("/ignoreFriend/{securityKey}/{friendid}")
   //@Produces(MediaType.APPLICATION_XML)
   public int ignoreFriend(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.ignoreFriend(securityKey, friendid);
   }
   
   @GET
   @Path("/restoreFriend/{securityKey}/{ulidForFriendship}")
   //@Produces(MediaType.APPLICATION_XML)
   public int restoreFriend(@PathParam("securityKey") String securityKey,@PathParam("ulidForFriendship") int ulidForFriendship) throws SQLException, ClassNotFoundException{
      return friendManager.ignoreFriend(securityKey, ulidForFriendship);
   }
   
   @GET
   @Path("/addFriend/{securityKey}/{friendEmail}")
   public String addFriend(@PathParam("securityKey") String securityKey, @PathParam("friendEmail") String friendEmail) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   friendEmail = URLDecoder.decode(friendEmail,"UTF-8");
	   return friendManager.addFriend(securityKey, friendEmail);
   }
   
   /*
   @GET
   @Path("/getFriendshipRequistUserLinkId/{securityKey}/{friendid}")
   //@Produces(MediaType.APPLICATION_XML)
   public int getFriendshipRequistUserLinkId(@PathParam("securityKey") String securityKey,@PathParam("friendid") int friendid) throws SQLException, ClassNotFoundException{
      return friendManager.getFriendshipRequestUserLinkId(securityKey, friendid);
   }
   */
   
   @GET
   @Path("/approveFriendshipRequest/{ulidForFriendship}")
   @Produces(MediaType.APPLICATION_XML)
   public int approveFriendshipRequest(@PathParam("ulidForFriendship") int ulidForFriendship) throws SQLException, ClassNotFoundException{
      return friendManager.approveFriendshipRequest(ulidForFriendship);
   }
}