(function () {
    'use strict';

angular.module('scopeRegistration', []).controller('RegController', ['$scope', '$http', function($scope, $http) {

var securityKey = cookieGet("securityKey");
var host = cookieGet("host");
var method = "GET";
var url = "http://" + host + "/ChatRest/rest/AccountService/getAccountData/"+securityKey;
var responsePromise = $http.get(url);	
responsePromise.success
	(function(data, status, headers, config) {
	var x2js1 = new X2JS();
	var jsonResponse = x2js1.xml_str2json(data);
    $scope.fname = jsonResponse.accounts.account.firstName;
    $scope.lname = jsonResponse.accounts.account.lastName;
    $scope.email = jsonResponse.accounts.account.email;
	});
responsePromise.error(function(data, status, headers, config) {
    alert("Request failed!");
});
	
$scope.reset = function () {
	$scope.oldPassword = null;
    $scope.password = null;
    $scope.passwordConfirm = null;
    $scope.regForm.$setUntouched();
};

$scope.resetName = function () {
   $scope.fname = null;
   $scope.lname = null;
   var responsePromise = $http.get(url);	
   		responsePromise.success
   		(function(data, status, headers, config) {
   			var x2js1 = new X2JS();
   			var jsonResponse = x2js1.xml_str2json(data);
   			$scope.fname = jsonResponse.accounts.account.firstName;
   			$scope.lname = jsonResponse.accounts.account.lastName;
   			$scope.email = jsonResponse.accounts.account.email;
   		});
   		responsePromise.error(function(data, status, headers, config) {
   			alert("Request failed!");
   		});
   $scope.regForm.$setUntouched();
   $scope.NameValidate= "";
   $scope.NameError="";
};

$scope.updateName = function () {	
	var email = escape($scope.email,"UTF-8");
	var fname = $scope.fname;
	var lname = $scope.lname;
	
	var urlModifyName = "http://" + host + "/ChatRest/rest/AccountService/updateAccountName/"+securityKey+"/"+fname+"/"+lname;
	var responsePromise1 = $http.get(urlModifyName);	
	responsePromise1.success
		(function(data, status, headers, config) {
			if (data = 1){
				$scope.NameValidate = "Updated. System will apply changes with the next log in";
			}
			else
	    	{
				$scope.friendAddError = "Update Failed";
	    	}
	    	});
	    responsePromise1.error(function(data, status, headers, config) {
	        $scope.friendAddError = "Request failed!";
	    });	
};

$scope.updatePassword = function () {	
	var oldPassword= escape($scope.oldPassword,"UTF-8");
	var password = escape($scope.password,"UTF-8");
	var passwordConfirm = escape($scope.passwordConfirm,"UTF-8");
	
	var urlModifyName = "http://" + host + "/ChatRest/rest/AccountService/updateAccountName/"+securityKey+"/"+fname+"/"+lname+"/"+email;
	var responsePromise1 = $http.get(urlModifyName);	
	responsePromise1.success
		(function(data, status, headers, config) {
			if (data = 1){
				$scope.PwdValidate = "Updated";
				$scope.oldPassword = null;
			    $scope.password = null;
			    $scope.passwordConfirm = null;
			    $scope.oldPassword.$setUntouched();
			    $scope.password.$setUntouched();
			    $scope.passwordConfirm.$setUntouched();
			}
			else
	    	{
				$scope.PwdError = "Update Failed";
	    	}
	    	});
	    responsePromise1.error(function(data, status, headers, config) {
	        $scope.PwdError = "Request failed!";
	    });	
	};
	
	$scope.deleteAccount = function () {
		var urlDeleteAccount = "http://" + host + "/ChatRest/rest/AccountService/deleteAccount/" + securityKey;
		
		var responsePromise2 = $http.get(urlDeleteAccount);	
			responsePromise2.success
			(function(data, status, headers, config) {
				if (data = 1){
					eraseCookieFromAllPaths("uid");
					eraseCookieFromAllPaths("host");
				  	alert = "Your account deleted";
					window.open('../login/login.view.html','_self');
				}
				else
		    	{
					alert("Something went wrong... Please, stay with us");
					
		    	}
		    	});
		    responsePromise2.error(function(data, status, headers, config) {
		        alert("Something went wrong with our database... Please, stay with us");
		    });	
	};

}]);
}());