(function () {
    'use strict';

// sorting: https://scotch.io/tutorials/sort-and-filter-a-table-using-angular

angular.module('myAppName', []).controller('ContactController', ['$scope', '$http', '$interval', function($scope, $http, $interval) {
	var securityKey = cookieGet("securityKey");	
	var host = cookieGet("host");
	//var host = "localhost:8080";
	$scope.newcontactEmail = "Your friend e-mail";
	//onfocus="if(this.value == 'Your friend e-mail') { this.value = ''; }" value="Your friend e-mail"


$scope.getAllFriends = function getAllFriends(){
// GET ALL FRIENDS LIST
var urlAllFriends = "http://" + host + "/ChatRest/rest/FriendService/allFriends/"+securityKey;
var responsePromise1 = $http.get(urlAllFriends);	
responsePromise1.success
	(function(data, status, headers, config) {
		/*
		XML to JSON conversion
		var xmlResponse1 = data;
		var x2js1 = new X2JS();
		var jsonResponse1 = x2js1.xml_str2json(xmlResponse1);
	    $scope.contacts = jsonResponse1.friends.friend;
	    */
		$scope.contacts = data;
	});
responsePromise1.error
	(function(data, status, headers, config) {
	alert("Request failed!");
});	
};

$scope.viewMesseges = function(friendid, firstName, lastName) {
	var urlViewMessages = "http://" + host + "/ChatRest/rest/MessageService/getDialog/"+securityKey+"/"+friendid;
	
	var responsePromise2 = $http.get(urlViewMessages);	
		responsePromise2.success
		(function(data, status, headers, config) {
			/*
			XML to JSON conversion
			var xmlResponse1 = data;
			var x2js1 = new X2JS();
			var jsonResponse1 = x2js1.xml_str2json(xmlResponse1);
		    var resultJson = jsonResponse2.messages.message;
		    */
			$scope.messages = data;
			if (firstName != "same") {
				fullNameForDialog = "Dialog with " + firstName + " " + lastName;
				$scope.dialogWithFriend = fullNameForDialog;
			};
		    $scope.hiddenFriendId = friendid;
		    //document.getElementById('dialogForm').style.visibility = 'visible';
		    //document.getElementById('selectDialogForm').style.visibility = 'hidden';
		});
		responsePromise2.error(function(data, status, headers, config) {
	        alert("Request failed!");
	    });	
};

$scope.sendMessage = function() {
	var friendid = $scope.hiddenFriendId;
	var messageText = $scope.messageText;
	var urlSendMessage = "http://" + host + "/ChatRest/rest/MessageService/sendMessage/" + securityKey + "/" + friendid + "/" + messageText;
	var responsePromise4 = $http.get(urlSendMessage);	
		responsePromise4.success
		(function(data, status, headers, config) {
			$scope.messageText = null;
		    //$scope.sendMessageValid = "Message Sent";
			$scope.viewMesseges(friendid);
		});
		responsePromise4.error(function(data, status, headers, config) {
	        alert("Request failed!");
	    });	
};

$scope.deleteMessage = function(messageid) {
	var friendid = $scope.hiddenFriendId;
	var urlDeleteMessage = "http://" + host + "/ChatRest/rest/MessageService/deleteMessage/" + securityKey + "/" + friendid + "/" + messageid;
	
	var responsePromise3 = $http.get(urlDeleteMessage);	
		responsePromise3.success
		(function(data, status, headers, config) {
			/*
			XML to JSON conversion
			var xmlResponse1 = data;
			var x2js1 = new X2JS();
			var jsonResponse1 = x2js1.xml_str2json(xmlResponse1);
		    var resultJson = jsonResponse2.messages.message;
		    */
			$scope.messages = data;
		});
		responsePromise3.error(function(data, status, headers, config) {
	        alert("Request failed!");
	    });	
};

$scope.ignoreFriend = function(id) {
	var urlIgnoreFriend = "http://" + host + "/ChatRest/rest/FriendService/ignoreFriend/" + securityKey +"/" + id;
	
	if (confirm("Do you want to ignore friend?") == true) {
		var responsePromise5 = $http.get(urlIgnoreFriend);	
		responsePromise5.success
		(function(data, status, headers, config) {
			if (data = 1){
				$scope.friendIgnoreValid = "Ignored since this moment";
				$scope.getAllFriends();
			}
			else
	    	{
				$scope.friendIgnoreError = "You can't ignore this guy";
	    	}
	    	});
	    responsePromise5.error(function(data, status, headers, config) {
	        alert("Request failed!");
	    });
    } else {
        //cancel
    }
}; 

$scope.reset = function () {
    $scope.messageText = null;
   // $scope.regForm.$setUntouched();
};

/*
 * CALL FOR FUNCTIONS ON PAGE LOAD
 */
var fullNameForDialog = "";
$scope.dialogWithFriend = "Please select one of your friends (or this is a great place for your advertisement. Just kidding)";
$scope.getAllFriends();

var counterOfRequestsFriends = 0;
var timerFriends=$interval(function(){
	if(counterOfRequestsFriends > 3)
      {
    	$scope.autoRefeshStatus = "Please refresh dialog manually: ";
      }
    else{
    	$scope.getAllFriends();	
    	counterOfRequestsFriends++;
    }
  },50000);

var counterOfRequestsMessages=0;
var timer=$interval(function(){
	var friendid = $scope.hiddenFriendId;
	if (friendid > 0){
		if(counterOfRequestsMessages > 3)
	      {
	    	$scope.autoRefeshStatus = "Please refresh dialog manually: ";
	      }
		else{
			$scope.viewMesseges(friendid,"same","same");
			counterOfRequestsMessages++;
		}
	}
  },10000);

$scope.autoRefresh = function () {
   $scope.autoRefeshStatus = "";
   counterOfRequestsFriends = 0;
   counterOfRequestsMessages = 0;
   // $scope.regForm.$setUntouched();
};

}]);
}());

/*
SAVE CONTACT FUNCTION
$scope.saveContact = function() {
    var i = 0;    
	if($scope.newcontact.id == null) {
             $scope.newcontact.id = uid++;
             $scope.contacts.push($scope.newcontact);
        } else {
            
             for(i in $scope.contacts) {
                    if($scope.contacts[i].id == $scope.newcontact.id) {
                        $scope.contacts[i] = $scope.newcontact;
                    }
             }                
        }
        $scope.newcontact = {};
};
*/

/*
SHOW CONFIRM BUTTON POP UP WINDOW WITH BUTTONS
$scope.showConfirm = function(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
          .title('Would you like to delete your debt?')
          .textContent('All of the banks have agreed to forgive you your debts.')
          .ariaLabel('Lucky day')
          .targetEvent(ev)
          .ok('Please do it!')
          .cancel('Sounds like a scam');
    $mdDialog.show(confirm).then(function() {
      $scope.status = 'You decided to get rid of your debt.';
    }, function() {
      $scope.status = 'You decided to keep your debt.';
    });
  };
*/	

/*
 * KILL AUTO UPDATES
$scope.killtimer=function(){
if(angular.isDefined(timer))
  {
    $interval.cancel(timer);
    timer=undefined;
    $scope.message="Timer is killed :-(";
  }
};
*/