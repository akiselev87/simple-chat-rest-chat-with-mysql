package rest.account;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/AccountService")
public class AccountService {

   AccountManager accountManager = new AccountManager();
   
   @GET
   @Path("/deleteAccount/{securityKey}")
   //@Produces(MediaType.APPLICATION_XML)
   //public List<Account> deleteAccount() throws SQLException, ClassNotFoundException{
   //  return accountManager.deleteAccount(2);
   public int deleteAccount(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
	  return accountManager.deleteAccount(securityKey);
   }
   
   @GET
   @Path("/getAccountData/{securityKey}")
   @Produces(MediaType.APPLICATION_XML)
   //public List<Account> deleteAccount() throws SQLException, ClassNotFoundException{
   //return accountManager.deleteAccount(2);
   public List<Account> getAccountData(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
	return accountManager.getAccountData(securityKey);
   }
   
   @GET
   @Path("/updateAccountName/{securityKey}/{fname}/{lname}")
   //@Produces(MediaType.APPLICATION_XML)
   public int updateAccountName(@PathParam("securityKey") String securityKey,@PathParam("fname") String fname,@PathParam("lname") String lname) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   return accountManager.updateAccountName(securityKey, fname, lname);
   }
   
   @GET
   @Path("/updateAccountPwd/{securityKey}/{oldpwd}/{pwd}/{pwdconfirm}")
   @Produces(MediaType.APPLICATION_XML)
   //public List<Account> deleteAccount() throws SQLException, ClassNotFoundException{
   //  return accountManager.deleteAccount(2);
   public int updateAccountPwd(@PathParam("securityKey") String securityKey,@PathParam("oldpwd") String oldpwd,@PathParam("pwd") String pwd,@PathParam("pwdconfirm") String pwdconfirm) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   oldpwd = URLDecoder.decode(oldpwd,"UTF-8");
	   pwd = URLDecoder.decode(pwd,"UTF-8");
	   pwdconfirm = URLDecoder.decode(pwdconfirm,"UTF-8");
	   return accountManager.updateAccountPwd(securityKey, oldpwd, pwd, pwdconfirm);
   }
   
   @GET
   @Path("/getAccountFullName/{securityKey}")
   public String getAccountFullName(@PathParam("securityKey") String securityKey) throws SQLException, ClassNotFoundException{
	return accountManager.getAccountFullName(securityKey);
   }
      
   @GET
   @Path("/login/{email}/{password}")
   //@Produces(MediaType.APPLICATION_XML)
   //public List<Account> deleteAccount() throws SQLException, ClassNotFoundException{
   //  return accountManager.deleteAccount(2);
   public String login(@PathParam("email") String email,@PathParam("password") String password) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   email = URLDecoder.decode(email,"UTF-8");
	   password = URLDecoder.decode(password,"UTF-8");
	   return accountManager.login(email, password);
   }
      
   @GET
   @Path("/verifyThatEmailsFreeForRegistration/{email}")
   @Produces(MediaType.APPLICATION_XML)
   //public List<Account> deleteAccount() throws SQLException, ClassNotFoundException{
   //  return accountManager.deleteAccount(2);
   public int verifyThatEmailsFreeForRegistration(@PathParam("email") String email) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   email = URLDecoder.decode(email,"UTF-8");
	   return accountManager.verifyThatEmailsFreeForRegistration(email);
   }
   
   @GET
   @Path("/register/{fname}/{lname}/{email}/{pwd}/{pwdconfirm}")
   @Produces(MediaType.APPLICATION_XML)
   //public List<Account> deleteAccount() throws SQLException, ClassNotFoundException{
   //  return accountManager.deleteAccount(2);
   public String register(@PathParam("fname") String fname,@PathParam("lname") String lname,@PathParam("email") String email,@PathParam("pwd") String pwd,@PathParam("pwdconfirm") String pwdconfirm) throws SQLException, ClassNotFoundException, UnsupportedEncodingException{
	   email = URLDecoder.decode(email,"UTF-8");
	   pwd = URLDecoder.decode(pwd,"UTF-8");
	   pwdconfirm = URLDecoder.decode(pwdconfirm,"UTF-8");
	   return accountManager.register(fname, lname, email, pwd, pwdconfirm);
   }
   
}