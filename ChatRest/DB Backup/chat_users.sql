-- MySQL dump 10.13  Distrib 5.7.9, for Win32 (AMD64)
--
-- Host: localhost    Database: chat
-- ------------------------------------------------------
-- Server version	5.6.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `email` varchar(45) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `user_password` varchar(45) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('aa@aa.aaaa','First','Last','1qaz!QAZ',2),('cc@cc.cc','User','3','1qaz!QAZ',3),('dd@dd.dd','User','4','1qaz!QAZ',4),('tt@tt.tt','tt','tt','1qaz!QAZ',5),('ii@ii.ii','ii','ii','1qaz!QAZ',6),('@ii.ii','ii','ii','1qaz!QAZ',8),('gg2@gg.gg','gg','gg','1qaz!QAZ',10),('gg@gg.gg','gg','gg','1qaz!QAZ',11),('nn@nn.nn','nn','nn','1qaz!QAZ',12),('ss@ss.ss','ss','ss','1qaz!QAZ',14),('jj@jj.jj','jj','jj','1qaz!QAZ',15),('kk@kk.kk','kk','kk','1qaz!QAZ',16),('hh@hh.hh','hh','hh','1qaz!QAZ',17),('nnn@nnn.nnn','nnn','nnn','1qaz!QAZ',18),('aaa@aaa.aaa','aaa','aaa','1qaz!QAZ',19),('vvv@vvv.vvv','vvv','vvv','1qaz!QAZ',20),('ccc@ccc.ccc','ccc','ccc','1qaz!QAZ',21),('bbb@bbb.bbb','bbb','bbb','1qaz!QAZ',22),('yyy@yyy.yyy','yyy','yyy','1qaz!QAZ',23),('iii@iii.iii','iii','iii','1qaz!QAZ',24),('mmm@mmm.mmm','mmm','mmm','1qaz!QAZ',25),('ppp@ppp.ppp','ppp','ppp','1qaz!QAZ',26),('nnnn@nnnn.nnn','nnn','nnnn','1qaz!QAZ',27),('akisv87@gmail.com','Alexey','Kiselev','1qaz!QAZ',29),('mm@mm.mm','mm','mm','1qaz!QAZ',30);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-28 16:21:36
