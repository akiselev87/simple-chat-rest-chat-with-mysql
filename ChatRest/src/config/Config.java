package config;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import rest.account.Account;

public class Config {
	private SecureRandom random = new SecureRandom();
	
	public Statement getStatement() throws SQLException, ClassNotFoundException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection connection = DriverManager.getConnection	("jdbc:mysql://localhost:3306/chat","root","1qaz!QAZ");
		Statement statement = connection.createStatement() ;
		return statement;
	}
	
	public String nextSessionId() {
		return new BigInteger(130, random).toString(32);
	}
	
	
	public int getUserIdBySessionId(String securityKey) throws ClassNotFoundException, SQLException{
		int userid = -1;
		Statement statement = getStatement();
		ResultSet resultset = statement.executeQuery("SELECT id_user FROM sessionsecurity WHERE security_key ='"+securityKey+"' ORDER BY id DESC;");
		if (resultset.first()){
			userid = resultset.getInt("id_user");
		}
		else
		{
			//error
		}
		return userid;
	}
	
	public String getSessionIdByEmailAndPassword(String email, String password) throws SQLException, ClassNotFoundException {
		String securityKey = "-1";
		Config config = new Config();
		Statement statement = config.getStatement();
		
		ResultSet rs;
	    rs = statement.executeQuery("SELECT sessionsecurity.security_key FROM sessionsecurity "
	    		+ "JOIN users ON sessionsecurity.id_user = users.id "
	    		+ "where users.email='" + email + "' and users.user_password='" + password + ""
	    		+ "' ORDER BY sessionsecurity.id DESC");
	    if (rs.first()){
	    	securityKey = rs.getString("security_key");
		    //succesfullyLogined = 1;
	    	//session.setAttribute("userid", userid);
	    } else {
	    }
	    return securityKey;
	}
	
	public String setSessionId(int userId) throws SQLException, ClassNotFoundException{
		String sessionId = nextSessionId();
		String sqlquery = "insert into sessionsecurity (id_user,security_key) values ('" + userId + "','" + sessionId + "');";
		Statement statement = getStatement();
		statement.executeUpdate(sqlquery);
   		return sessionId;
	}
}