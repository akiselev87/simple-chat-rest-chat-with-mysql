package rest.account;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import config.Config;

public class AccountManager {
	
	public int deleteAccount(String securityKey) throws SQLException, ClassNotFoundException {
	Config config = new Config();
	Statement statement = config.getStatement();
	int useridtodelete = config.getUserIdBySessionId(securityKey);	
	statement.executeUpdate("delete from users where id = '" + useridtodelete + "'");
	return 1;
	}
	
	public List<Account> getAccountData(String securityKey) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String email = "";
		
		ArrayList<Account> accountData= null;
		accountData = new ArrayList<Account>();
		
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery("SELECT users.first_name,users.last_name, users.email FROM chat.users where users.id="+userid+";");
		while (resultset.next()){
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			email = resultset.getString("email");
			
			Account account = new Account(userid, fname, lname, email);
			accountData.add(account);
		}
		return accountData;
	}
	
	public int updateAccountName(String securityKey, String fname, String lname) throws SQLException, ClassNotFoundException {
		int status = -1;
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
		
		if (userid > 0 && fname != "" && lname!=""){
			status = statement.executeUpdate(
		"UPDATE users SET first_name = '" + fname +"', last_name = '" + lname + "' WHERE id = " + userid + ";");
		}
		return status;
	}
	
	public int updateAccountPwd(String securityKey, String oldpwd, String pwd, String pwdconfirm) throws SQLException, ClassNotFoundException {
		int status = -1;
		ArrayList<Account> accountData= null;
		accountData = new ArrayList<Account>();
		
		Config config = new Config();
		int userid = config.getUserIdBySessionId(securityKey);
		Statement statement = config.getStatement();
		if (userid > 0 && oldpwd != "" && pwd !="" && pwdconfirm != ""
			&& pwd.equals(pwdconfirm)){
			
			ResultSet rs;
		    rs = statement.executeQuery("select * from users where id='" + userid + "' and user_password='" + oldpwd + "'");
		    if (rs.first()){
		    	status = statement.executeUpdate("UPDATE users SET user_password = '" + pwd + "' WHERE id = " + userid + ";");
		    }
		}
		return status;
}
	
	public String getAccountFullName(String securityKey) throws SQLException, ClassNotFoundException {
		String fname = "";
		String lname = "";
		String email = "";
		String fullName = "";
		
		Config config = new Config();
		Statement statement = config.getStatement();
		int userid = config.getUserIdBySessionId(securityKey);
		
		ResultSet resultset = statement.executeQuery("SELECT users.first_name,users.last_name, users.email FROM chat.users where users.id="+userid+";");
		while (resultset.next()){
			fname = resultset.getString("first_name");
			lname = resultset.getString("last_name");
			fullName = fname + " " + lname;
		}
		return fullName;
	}
	
	public String login(String email, String password) throws SQLException, ClassNotFoundException {
	String securityKey = "";
	Config config = new Config();
	Statement statement = config.getStatement();
	
	ResultSet rs;
    rs = statement.executeQuery("select * from users where email='" + email + "' and user_password='" + password + "'");
    if (rs.first()){
    	int userId = rs.getInt("id");
    	securityKey = config.setSessionId(userId);
    } else {
        //error
	}
    return securityKey;
	}
	
	
	public int verifyThatEmailsFreeForRegistration(String email) throws SQLException, ClassNotFoundException {
		int duplicatedEmailFound = 0;
		
		Config config = new Config();
		Statement statement = config.getStatement();
		
		ResultSet rs;
	    //matches should be found at sql statement TO BE DEVELOPED
		rs = statement.executeQuery("Select email From users where email='"+email+"';");
	    if (rs.first()){
	    	//userid = rs.getInt("id");
	    	duplicatedEmailFound = 1;
	    }
	    return duplicatedEmailFound;
	}
	
	public String register(String fname, String lname, String email, String pwd, String pwdConfirm ) throws SQLException, ClassNotFoundException {
		int errorexists = 0;
		String errorText = "";
		
		Config config = new Config();
		Statement statement = config.getStatement();
		
		//Features:
		//- check that all fields are correct (not null, passwords matches, patters are correct)
		//- check that user is not already added
		//- add new user to the system

	  	//matchers and patterns
	  	Pattern pattern;
	  	Matcher matcher;
	  	String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	  	String PWD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+]).{6,20})"; 
	  	
	  	//Check that fields are not null
	  	if(fname.equals("")){
	  		errorexists = 1;
	  		errorText = "Please, add First Name. ";
	  	}
	  	
	  	if(lname.equals("")){
	  		errorexists = 1;
	  		errorText = errorText + "<Please, add Last Name. ";
	  	}
	  	
	  	if(email.equals("")){
	  		errorexists = 1;
	  		errorText = errorText + "Please, add E-Mail. ";
	  	}
	  	else 
	  	//check e-mail pattern
	  	{
	  		pattern = Pattern.compile(EMAIL_PATTERN);
	      	matcher = pattern.matcher(email);
	      	if (!matcher.matches()) {
	      		errorText = errorText + "Please, verify e-mail format. ";
	      		errorexists = 1;
	      	}
	  	}	
	  	
	  	//check that passwords exists 
	  	if(pwd.equals("") && pwdConfirm.equals("")){
	  		errorexists = 3;
	  		errorText = errorText + "Please, add Password and Confirmation Password. ";
	  	}
	  	else 
	  	//check password pattern
	  	{	pattern = Pattern.compile(PWD_PATTERN);
	  		matcher = pattern.matcher(pwd);
	  		if (!matcher.matches()) {
	  			errorText = errorText + "Password requirements: password between 6 to 20 characters which contain at least one numeric digit, onwe upper case lettwr, one lower case letter and a special character. ";
	  			errorexists = 1;
	  		}
	  	}
	  	
	  	//confirm that both passwords equals
	  	if ((errorexists != 3) && !pwdConfirm.equals(pwd)) {
	  		errorexists = 1;
	  		errorText = errorText + "Confirm password is not the same as password. ";
	    }
	  	
	  	//verify that e-mail is unique
	  	if (errorexists == 0){
	  		if (verifyThatEmailsFreeForRegistration(email) == 1){
	  			errorText = errorText + "Account with the same e-mail is already exists. ";
	  			errorexists = 1;
	  		}
	  	}
	  	
	  	//Create a new user
	   	if (errorexists == 0){
	   		String sqlquery = "insert into users(first_name,last_name,email,user_password) values ('" + fname + "','" + lname + "','" + email + "','" + pwd + "');";
	   		try{
	   			
	   			statement.executeUpdate(sqlquery);
	   		}
	   		catch (Exception ex )
	   		{
	   			throw new SQLException(sqlquery + "\n" + ex.getMessage());
	   			
	   		}
	   	}
	   	return errorText;
	    //Debug message  
	    //  out.println("<BR> Results: ");
	    //  ResultSet resultset = statement.executeQuery("Select * From users"); 
	    //  while(resultset.next()){
	    //  	out.println("<BR>");
	    //  	out.println(resultset.getString("first_name"));
	    //  }	
		}


}
	