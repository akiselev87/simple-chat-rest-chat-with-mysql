(function () {
    'use strict';

angular.module('scopeRegistration', []).controller('RegController', ['$scope', '$http', function($scope, $http) {
var securityKey = cookieGet("securityKey");
var host = cookieGet("host");
var method = "GET";
var url = "http://" + host + "/ChatRest/rest/AccountService/getAccountFullName/"+securityKey;
$scope.iframeurl="../messages/messages.view.html";
	
var responsePromise = $http.get(url);	
responsePromise.success
	(function(data, status, headers, config) {
	var textResponse = data;
	$scope.currentUserName = textResponse;
	});
responsePromise.error(function(data, status, headers, config) {
    alert("Request failed!");
});	
 
$scope.logout = function () {
	eraseCookieFromAllPaths("uid");
	eraseCookieFromAllPaths("host");
  	window.open('../login/login.view.html','_self');
};

}]);
}());