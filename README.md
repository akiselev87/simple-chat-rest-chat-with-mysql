Simple Chat is a 3 tier Web application:

- The DB has been developed on MySQL Workbench 6.3.

- As a Backend I used Apache Tomcat with Java and JDBC, Server responds with XML or JSON (I used both format to show the difference, but JSON is preferrable for AngularJS) by HTTP GET.

- The Frontend is AngularJS with JavaScript, which provides great flexibility (I didn't use bootstrap library and all CSS file is a custom one).