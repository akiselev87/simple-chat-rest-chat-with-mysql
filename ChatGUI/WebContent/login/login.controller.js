(function () {
    'use strict';

angular.module('login', []).controller('LoginController', ['$scope', '$http', function($scope, $http) {
    	
$scope.register = function () {
var email = escape($scope.email,"UTF-8");
var pwd = escape($scope.password,"UTF-8");
var host = "localhost:8080";

var urlLogin = "http://" + host + "/ChatRest/rest/AccountService/login/"+email+"/"+pwd;
var responsePromise = $http.get(urlLogin);	

responsePromise.success
	(function(data, status, headers, config) {
		var securityKey = data;
		if (securityKey != ""){
			cookiePost("securityKey", securityKey , 1);
			cookiePost("host", host , 1);
			window.open('../index/index.view.html','_self');
		}
		else
		{
			$scope.message = "Login/Password pair error. Please try again...";
		}
		});
    responsePromise.error(function(data, status, headers, config) {
        alert("Something went wrong with our database... Please, try again later");
    });	
};
}]);
}());