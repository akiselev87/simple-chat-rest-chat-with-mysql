package xml.message;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "message")
public class Message implements Serializable {

   private static final long serialVersionUID = 1L;
   private int id;
   private String firstName;
   private String lastName;   
   private String text;
   private int authorId;

   public Message(){}
   
   public Message(int id, String firstName, String lastName, String text, int authorId){
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.text = text;
      this.authorId = authorId;
   }
   //------------------------------
   public int getId() {
      return id;
   }

   @XmlElement
   public void setId(int id) {
      this.id = id;
   }
   
   //------------------------------
   public String getFirstName() {
      return firstName;
   }
   @XmlElement
   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }
 
   //------------------------------
   public String getLastName() {
	      return lastName;
	   }
   @XmlElement
   public void setLastName(String lastName) {
   this.lastName = lastName;
   }
   //------------------------------	   
   public String getText() {
      return text;
   }
   @XmlElement
   public void setText(String text) {
      this.text = text;
   }
 ///------------------------------
   public int getAuthorId() {
	  return authorId;
   }
  @XmlElement
   public void setAuthorId(int authorId) {
      this.authorId = authorId;
   }
}