package rest.friends;

import java.io.Serializable;
//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;
//@XmlRootElement(name = "friend")

public class Friend implements Serializable {

   private static final long serialVersionUID = 1L;
   private int id;
   private String firstName;
   private String lastName;   
   private String email;

   public Friend(){}
   
   public Friend(int id, String firstName, String lastName, String email){
      this.id = id;
      this.firstName = firstName;
      this.lastName = lastName;
      this.email = email;
   }
   //------------------------------
   public int getId() {
      return id;
   }

   //@XmlElement
   public void setId(int id) {
      this.id = id;
   }
   
   //------------------------------
   public String getFirstName() {
      return firstName;
   }
   //@XmlElement
   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }
 
   //------------------------------
   public String getLastName() {
	      return lastName;
	   }
   //@XmlElement
   public void setLastName(String lastName) {
   this.lastName = lastName;
   }
   //------------------------------	   
   public String getEmail() {
      return email;
   }
   //@XmlElement
   public void setEmail(String email) {
      this.email = email;
   }		
}